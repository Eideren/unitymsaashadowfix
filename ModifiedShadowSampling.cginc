// -Stripped down and modified version of Internal-ScreenSpaceShadows.cginc @ version 5.6.2f1
// -Eideren
// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)
// Collects cascaded shadows into screen space buffer
// -^ not really anymore since we are computing it on each lighting pass for each model's fragments instead
// -Eideren

#ifndef MODIFIED_SHADOW_SAMPLING
#define MODIFIED_SHADOW_SAMPLING

// -Force inclusion of AutoLight at this time to override a function defined in that file to the one below in this file.
// -If removed, AutoLight will be included further inside UnityStandardCore which will then override the function of this file instead !
// -It is then essential that this cginc is loaded as the absolute first cginc loaded to avoid other functions being -
// -written with unity's version of the function.
// -Eideren
#include "AutoLight.cginc"

// -Custom variables set over C#
// -Eideren
float4 G_SelectedLightDir;
UNITY_DECLARE_SHADOWMAP(G_ProxyShadowmapTexture);
float4 G_ProxyShadowmapTexture_TexelSize;


// -Overrides UNITY_LIGHT_ATTENUATION's DIRECTIONAL function by ours, this might produce a warning in the console on compile.
// -The way that we filter out wrong lights is by matching the direction of the light we selected,
// -really hacky, but we don't have access to the forward light pass rendering event -
// -(just the shadow mask or shadowmap ATM) to enable a shader keyword at that time.
// -Only override inside SHADOWS_SCREEN ; this seems to be unity's way to filter out shadowmap sampling when we disable - 
// -it or when we sample a backed one instead.
// -Eideren
#ifdef DIRECTIONAL
    #if defined (SHADOWS_SCREEN)
        // Removes warning about defines override
        #if defined (UNITY_LIGHT_ATTENUATION)
            #undef UNITY_LIGHT_ATTENUATION
        #endif
        #define UNITY_LIGHT_ATTENUATION(destName, input, worldPos) \
            fixed destName = 0; \
            UNITY_BRANCH \
            if(dot(G_SelectedLightDir.xyz, _WorldSpaceLightPos0.xyz) > 0.995) \
                destName = SamplePCF5x5(float4(worldPos, 1)); \
            else \
                destName = UNITY_SHADOW_ATTENUATION(input, worldPos);
    #endif
#endif


#include "UnityCG.cginc"
#if UNITY_VERSION >= 201730
    // We have to modify the content of this cginc to include our shadowmap instead.
    #include "UnityShadowLibrary.cginc"
#endif










// Configuration


// Should receiver plane bias be used? This estimates receiver slope using derivatives,
// and tries to tilt the PCF kernel along it. However, since we're doing it in screenspace
// from the depth texture, the derivatives are wrong on edges or intersections of objects,
// leading to possible shadow artifacts. So it's disabled by default.
#if UNITY_VERSION >= 201730
// See also UnityGetReceiverPlaneDepthBias in UnityShadowLibrary.cginc.
//#define UNITY_USE_RECEIVER_PLANE_BIAS
#else
#define UNITY_USE_RECEIVER_PLANE_BIAS 0
#define UNITY_RECEIVER_PLANE_MIN_FRACTIONAL_ERROR 0.05f
#endif


// Blend between shadow cascades to hide the transition seams?
#define UNITY_USE_CASCADE_BLENDING 0
#define UNITY_CASCADE_BLEND_DISTANCE 0.1

// sizes of cascade projections, relative to first one
float4 unity_ShadowCascadeScales;


//
// Keywords based defines
//
#if defined (SHADOWS_SPLIT_SPHERES)
	#define GET_CASCADE_WEIGHTS(wpos, z)    getCascadeWeights_splitSpheres(wpos)
#else
	#define GET_CASCADE_WEIGHTS(wpos, z)	getCascadeWeights( wpos, z )
#endif

#if defined (SHADOWS_SINGLE_CASCADE)
	#define GET_SHADOW_COORDINATES(wpos,cascadeWeights)	getShadowCoord_SingleCascade(wpos)
#else
	#define GET_SHADOW_COORDINATES(wpos,cascadeWeights)	getShadowCoord(wpos,cascadeWeights)
#endif

#if UNITY_VERSION < 201730
    // prototypes
    //inline float3 computeCameraSpacePosFromDepth(v2f i);
    inline fixed4 getCascadeWeights(float3 wpos, float z);		// calculates the cascade weights based on the world position of the fragment and plane positions
    inline fixed4 getCascadeWeights_splitSpheres(float3 wpos);	// calculates the cascade weights based on world pos and split spheres positions
    inline float4 getShadowCoord_SingleCascade( float4 wpos );	// converts the shadow coordinates for shadow map using the world position of fragment (optimized for single fragment)
    inline float4 getShadowCoord( float4 wpos, fixed4 cascadeWeights );// converts the shadow coordinates for shadow map using the world position of fragment
    half 		  sampleShadowmap_PCF5x5 (float4 coord);		// samples the shadowmap based on PCF filtering (5x5 kernel)
    half 		  unity_sampleShadowmap( float4 coord );		// sample shadowmap SM2.0+
#endif

inline float3 P_UnityCombineShadowcoordComponents3 (float2 baseUV, float2 deltaUV, float depth, float3 receiverPlaneDepthBias)
{
    float3 uv = float3(baseUV + deltaUV, depth + receiverPlaneDepthBias.z);
    uv.z += dot(deltaUV, receiverPlaneDepthBias.xy);
    return uv;
}
inline float3 P_UnityCombineShadowcoordComponents2 (float2 baseUV, float2 deltaUV, float depth, float2 receiverPlaneDepthBias)
{
	float3 uv = float3( baseUV + deltaUV, depth );
	uv.z += dot (deltaUV, receiverPlaneDepthBias);
	return uv;
}

/**
 * Gets the cascade weights based on the world position of the fragment.
 * Returns a float4 with only one component set that corresponds to the appropriate cascade.
 */
inline fixed4 getCascadeWeights(float3 wpos, float z)
{
    fixed4 zNear = float4( z >= _LightSplitsNear );
    fixed4 zFar = float4( z < _LightSplitsFar );
    fixed4 weights = zNear * zFar;
    return weights;
}

/**
 * Gets the cascade weights based on the world position of the fragment and the poisitions of the split spheres for each cascade.
 * Returns a float4 with only one component set that corresponds to the appropriate cascade.
 */
inline fixed4 getCascadeWeights_splitSpheres(float3 wpos)
{
    float3 fromCenter0 = wpos.xyz - unity_ShadowSplitSpheres[0].xyz;
    float3 fromCenter1 = wpos.xyz - unity_ShadowSplitSpheres[1].xyz;
    float3 fromCenter2 = wpos.xyz - unity_ShadowSplitSpheres[2].xyz;
    float3 fromCenter3 = wpos.xyz - unity_ShadowSplitSpheres[3].xyz;
    float4 distances2 = float4(dot(fromCenter0,fromCenter0), dot(fromCenter1,fromCenter1), dot(fromCenter2,fromCenter2), dot(fromCenter3,fromCenter3));
    fixed4 weights = float4(distances2 < unity_ShadowSplitSqRadii);
    weights.yzw = saturate(weights.yzw - weights.xyz);
    return weights;
}
half G_ShadowDrawDistance;
int G_ShadowCascades;

inline half getShadowmapEdgeMask(float3 wpos, float z)
{
    #if defined (SHADOWS_SPLIT_SPHERES)
        return saturate((1 - distance(wpos, _WorldSpaceCameraPos) / G_ShadowDrawDistance) * 5);
        // -This method avoid discarding parts of the far shadowmap compared to unity's version and the one above
        /*
        float3 pos = 0;
        float comp = 0;

        // -No idea why i can't just use G_ShadowCascades to directly access the right axis, doesn't seem to work
        // -Eideren
        UNITY_BRANCH
        if(G_ShadowCascades == 4)
        {
            pos = unity_ShadowSplitSpheres[3].xyz;
            comp = unity_ShadowSplitSqRadii[3];
        }
        else if(G_ShadowCascades == 2)
        {
            pos  = unity_ShadowSplitSpheres[1].xyz;
            comp = unity_ShadowSplitSqRadii[1];
        }
        else
        {
            pos = unity_ShadowSplitSpheres[0].xyz;
            comp = unity_ShadowSplitSqRadii[0];
        }

        float3 deltaFromCenter = wpos.xyz - pos;
        // viewdir to avoid blending the border of the shadowmap nearest to us
        float3 viewDir = wpos.xyz - _WorldSpaceCameraPos;
        // Change the first constant if you want a larger blend
        return saturate(saturate((comp - dot(deltaFromCenter, deltaFromCenter)) * 0.1) + saturate(-1.5 * dot(normalize(viewDir), normalize(deltaFromCenter))));*/
    #else
        // Change the constant if you want a larger blend
        return saturate((-z+G_ShadowDrawDistance)*0.5);
    #endif
}

/**
 * Returns the shadowmap coordinates for the given fragment based on the world position and z-depth.
 * These coordinates belong to the shadowmap atlas that contains the maps for all cascades.
 */
inline float4 getShadowCoord( float4 wpos, fixed4 cascadeWeights )
{
    float3 sc0 = mul (unity_WorldToShadow[0], wpos).xyz;
    float3 sc1 = mul (unity_WorldToShadow[1], wpos).xyz;
    float3 sc2 = mul (unity_WorldToShadow[2], wpos).xyz;
    float3 sc3 = mul (unity_WorldToShadow[3], wpos).xyz;
    float4 shadowMapCoordinate = float4(sc0 * cascadeWeights[0] + sc1 * cascadeWeights[1] + sc2 * cascadeWeights[2] + sc3 * cascadeWeights[3], 1);
#if defined(UNITY_REVERSED_Z)
    float  noCascadeWeights = 1 - dot(cascadeWeights, float4(1, 1, 1, 1));
    shadowMapCoordinate.z += noCascadeWeights;
#endif
    return shadowMapCoordinate;
}

/**
 * Same as the getShadowCoord; but optimized for single cascade
 */
inline float4 getShadowCoord_SingleCascade( float4 wpos )
{
    return float4( mul (unity_WorldToShadow[0], wpos).xyz, 0);
}











#if UNITY_VERSION >= 201730












#define SHADOWMAPSAMPLER_AND_TEXELSIZE_DEFINED








/**
* PCF tent shadowmap filtering based on a 3x3 kernel (optimized with 4 taps)
*/
half UNITY_PCF3x3(float4 coord, float3 receiverPlaneDepthBias)
{
    half shadow = 1;

#ifdef SHADOWMAPSAMPLER_AND_TEXELSIZE_DEFINED

    #ifndef SHADOWS_NATIVE
        // when we don't have hardware PCF sampling, fallback to a simple 3x3 sampling with averaged results.
        return UnitySampleShadowmap_PCF3x3NoHardwareSupport(coord, receiverPlaneDepthBias);
    #endif

    // tent base is 3x3 base thus covering from 9 to 12 texels, thus we need 4 bilinear PCF fetches
    float2 tentCenterInTexelSpace = coord.xy * G_ProxyShadowmapTexture_TexelSize.zw;
    float2 centerOfFetchesInTexelSpace = floor(tentCenterInTexelSpace + 0.5);
    float2 offsetFromTentCenterToCenterOfFetches = tentCenterInTexelSpace - centerOfFetchesInTexelSpace;

    // find the weight of each texel based
    float4 texelsWeightsU, texelsWeightsV;
    _UnityInternalGetWeightPerTexel_3TexelsWideTriangleFilter(offsetFromTentCenterToCenterOfFetches.x, texelsWeightsU);
    _UnityInternalGetWeightPerTexel_3TexelsWideTriangleFilter(offsetFromTentCenterToCenterOfFetches.y, texelsWeightsV);

    // each fetch will cover a group of 2x2 texels, the weight of each group is the sum of the weights of the texels
    float2 fetchesWeightsU = texelsWeightsU.xz + texelsWeightsU.yw;
    float2 fetchesWeightsV = texelsWeightsV.xz + texelsWeightsV.yw;

    // move the PCF bilinear fetches to respect texels weights
    float2 fetchesOffsetsU = texelsWeightsU.yw / fetchesWeightsU.xy + float2(-1.5,0.5);
    float2 fetchesOffsetsV = texelsWeightsV.yw / fetchesWeightsV.xy + float2(-1.5,0.5);
    fetchesOffsetsU *= G_ProxyShadowmapTexture_TexelSize.xx;
    fetchesOffsetsV *= G_ProxyShadowmapTexture_TexelSize.yy;

    // fetch !
    float2 bilinearFetchOrigin = centerOfFetchesInTexelSpace * G_ProxyShadowmapTexture_TexelSize.xy;
    shadow =  fetchesWeightsU.x * fetchesWeightsV.x * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.x, fetchesOffsetsV.x), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.y * fetchesWeightsV.x * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.y, fetchesOffsetsV.x), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.x * fetchesWeightsV.y * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.x, fetchesOffsetsV.y), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.y * fetchesWeightsV.y * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.y, fetchesOffsetsV.y), coord.z, receiverPlaneDepthBias));
#endif

    return shadow;
}

/**
* PCF tent shadowmap filtering based on a 5x5 kernel (optimized with 9 taps)
*/
half UNITY_PCF5x5(float4 coord, float3 receiverPlaneDepthBias)
{
    half shadow = 1;

#ifdef SHADOWMAPSAMPLER_AND_TEXELSIZE_DEFINED

    #ifndef SHADOWS_NATIVE
        // when we don't have hardware PCF sampling, fallback to a simple 3x3 sampling with averaged results.
        return UnitySampleShadowmap_PCF3x3NoHardwareSupport(coord, receiverPlaneDepthBias);
    #endif

    // tent base is 5x5 base thus covering from 25 to 36 texels, thus we need 9 bilinear PCF fetches
    float2 tentCenterInTexelSpace = coord.xy * G_ProxyShadowmapTexture_TexelSize.zw;
    float2 centerOfFetchesInTexelSpace = floor(tentCenterInTexelSpace + 0.5);
    float2 offsetFromTentCenterToCenterOfFetches = tentCenterInTexelSpace - centerOfFetchesInTexelSpace;

    // find the weight of each texel based on the area of a 45 degree slop tent above each of them.
    float3 texelsWeightsU_A, texelsWeightsU_B;
    float3 texelsWeightsV_A, texelsWeightsV_B;
    _UnityInternalGetWeightPerTexel_5TexelsWideTriangleFilter(offsetFromTentCenterToCenterOfFetches.x, texelsWeightsU_A, texelsWeightsU_B);
    _UnityInternalGetWeightPerTexel_5TexelsWideTriangleFilter(offsetFromTentCenterToCenterOfFetches.y, texelsWeightsV_A, texelsWeightsV_B);

    // each fetch will cover a group of 2x2 texels, the weight of each group is the sum of the weights of the texels
    float3 fetchesWeightsU = float3(texelsWeightsU_A.xz, texelsWeightsU_B.y) + float3(texelsWeightsU_A.y, texelsWeightsU_B.xz);
    float3 fetchesWeightsV = float3(texelsWeightsV_A.xz, texelsWeightsV_B.y) + float3(texelsWeightsV_A.y, texelsWeightsV_B.xz);

    // move the PCF bilinear fetches to respect texels weights
    float3 fetchesOffsetsU = float3(texelsWeightsU_A.y, texelsWeightsU_B.xz) / fetchesWeightsU.xyz + float3(-2.5,-0.5,1.5);
    float3 fetchesOffsetsV = float3(texelsWeightsV_A.y, texelsWeightsV_B.xz) / fetchesWeightsV.xyz + float3(-2.5,-0.5,1.5);
    fetchesOffsetsU *= G_ProxyShadowmapTexture_TexelSize.xxx;
    fetchesOffsetsV *= G_ProxyShadowmapTexture_TexelSize.yyy;

    // fetch !
    float2 bilinearFetchOrigin = centerOfFetchesInTexelSpace * G_ProxyShadowmapTexture_TexelSize.xy;
    shadow  = fetchesWeightsU.x * fetchesWeightsV.x * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.x, fetchesOffsetsV.x), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.y * fetchesWeightsV.x * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.y, fetchesOffsetsV.x), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.z * fetchesWeightsV.x * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.z, fetchesOffsetsV.x), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.x * fetchesWeightsV.y * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.x, fetchesOffsetsV.y), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.y * fetchesWeightsV.y * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.y, fetchesOffsetsV.y), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.z * fetchesWeightsV.y * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.z, fetchesOffsetsV.y), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.x * fetchesWeightsV.z * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.x, fetchesOffsetsV.z), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.y * fetchesWeightsV.z * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.y, fetchesOffsetsV.z), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.z * fetchesWeightsV.z * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.z, fetchesOffsetsV.z), coord.z, receiverPlaneDepthBias));
#endif

    return shadow;
}
/**
* PCF tent shadowmap filtering based on a 7x7 kernel (optimized with 16 taps)
*/
half UNITY_PCF7x7(float4 coord, float3 receiverPlaneDepthBias)
{
    half shadow = 1;

#ifdef SHADOWMAPSAMPLER_AND_TEXELSIZE_DEFINED

    #ifndef SHADOWS_NATIVE
        // when we don't have hardware PCF sampling, fallback to a simple 3x3 sampling with averaged results.
        return UnitySampleShadowmap_PCF3x3NoHardwareSupport(coord, receiverPlaneDepthBias);
    #endif

    // tent base is 7x7 base thus covering from 49 to 64 texels, thus we need 16 bilinear PCF fetches
    float2 tentCenterInTexelSpace = coord.xy * G_ProxyShadowmapTexture_TexelSize.zw;
    float2 centerOfFetchesInTexelSpace = floor(tentCenterInTexelSpace + 0.5);
    float2 offsetFromTentCenterToCenterOfFetches = tentCenterInTexelSpace - centerOfFetchesInTexelSpace;

    // find the weight of each texel based on the area of a 45 degree slop tent above each of them.
    float4 texelsWeightsU_A, texelsWeightsU_B;
    float4 texelsWeightsV_A, texelsWeightsV_B;
    _UnityInternalGetWeightPerTexel_7TexelsWideTriangleFilter(offsetFromTentCenterToCenterOfFetches.x, texelsWeightsU_A, texelsWeightsU_B);
    _UnityInternalGetWeightPerTexel_7TexelsWideTriangleFilter(offsetFromTentCenterToCenterOfFetches.y, texelsWeightsV_A, texelsWeightsV_B);

    // each fetch will cover a group of 2x2 texels, the weight of each group is the sum of the weights of the texels
    float4 fetchesWeightsU = float4(texelsWeightsU_A.xz, texelsWeightsU_B.xz) + float4(texelsWeightsU_A.yw, texelsWeightsU_B.yw);
    float4 fetchesWeightsV = float4(texelsWeightsV_A.xz, texelsWeightsV_B.xz) + float4(texelsWeightsV_A.yw, texelsWeightsV_B.yw);

    // move the PCF bilinear fetches to respect texels weights
    float4 fetchesOffsetsU = float4(texelsWeightsU_A.yw, texelsWeightsU_B.yw) / fetchesWeightsU.xyzw + float4(-3.5,-1.5,0.5,2.5);
    float4 fetchesOffsetsV = float4(texelsWeightsV_A.yw, texelsWeightsV_B.yw) / fetchesWeightsV.xyzw + float4(-3.5,-1.5,0.5,2.5);
    fetchesOffsetsU *= G_ProxyShadowmapTexture_TexelSize.xxxx;
    fetchesOffsetsV *= G_ProxyShadowmapTexture_TexelSize.yyyy;

    // fetch !
    float2 bilinearFetchOrigin = centerOfFetchesInTexelSpace * G_ProxyShadowmapTexture_TexelSize.xy;
    shadow  = fetchesWeightsU.x * fetchesWeightsV.x * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.x, fetchesOffsetsV.x), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.y * fetchesWeightsV.x * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.y, fetchesOffsetsV.x), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.z * fetchesWeightsV.x * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.z, fetchesOffsetsV.x), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.w * fetchesWeightsV.x * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.w, fetchesOffsetsV.x), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.x * fetchesWeightsV.y * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.x, fetchesOffsetsV.y), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.y * fetchesWeightsV.y * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.y, fetchesOffsetsV.y), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.z * fetchesWeightsV.y * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.z, fetchesOffsetsV.y), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.w * fetchesWeightsV.y * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.w, fetchesOffsetsV.y), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.x * fetchesWeightsV.z * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.x, fetchesOffsetsV.z), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.y * fetchesWeightsV.z * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.y, fetchesOffsetsV.z), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.z * fetchesWeightsV.z * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.z, fetchesOffsetsV.z), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.w * fetchesWeightsV.z * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.w, fetchesOffsetsV.z), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.x * fetchesWeightsV.w * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.x, fetchesOffsetsV.w), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.y * fetchesWeightsV.w * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.y, fetchesOffsetsV.w), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.z * fetchesWeightsV.w * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.z, fetchesOffsetsV.w), coord.z, receiverPlaneDepthBias));
    shadow += fetchesWeightsU.w * fetchesWeightsV.w * UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents3(bilinearFetchOrigin, float2(fetchesOffsetsU.w, fetchesOffsetsV.w), coord.z, receiverPlaneDepthBias));

#endif

    return shadow;
}










#undef SHADOWMAPSAMPLER_AND_TEXELSIZE_DEFINED


























#endif










































































































#if UNITY_VERSION < 201730
/**
 * Computes the receiver plane depth bias for the given shadow coord in screen space.
 * Inspirations:
 *		http://mynameismjp.wordpress.com/2013/09/10/shadow-maps/
 *		http://amd-dev.wpengine.netdna-cdn.com/wordpress/media/2012/10/Isidoro-ShadowMapping.pdf
 */
float2 getReceiverPlaneDepthBias (float3 shadowCoord)
{
	float2 biasUV;
	float3 dx = ddx (shadowCoord);
	float3 dy = ddy (shadowCoord);

	biasUV.x = dy.y * dx.z - dx.y * dy.z;
    biasUV.y = dx.x * dy.z - dy.x * dx.z;
    biasUV *= 1.0f / ((dx.x * dy.y) - (dx.y * dy.x));
    return biasUV;
}

/**
 * PCF shadowmap filtering based on a 3x3 kernel (optimized with 4 taps)
 *
 * Algorithm: http://the-witness.net/news/2013/09/shadow-mapping-summary-part-1/
 * Implementation example: http://mynameismjp.wordpress.com/2013/09/10/shadow-maps/
 */
half sampleShadowmap_PCF3x3 (float4 coord, float2 receiverPlaneDepthBias)
{
	const float2 offset = float2(0.5,0.5);
	float2 uv = (coord.xy * G_ProxyShadowmapTexture_TexelSize.zw) + offset;
	float2 base_uv = (floor(uv) - offset) * G_ProxyShadowmapTexture_TexelSize.xy;
	float2 st = frac(uv);

	float2 uw = float2( 3-2*st.x, 1+2*st.x );
	float2 u = float2( (2-st.x) / uw.x - 1, (st.x)/uw.y + 1 );
	u *= G_ProxyShadowmapTexture_TexelSize.x;

	float2 vw = float2( 3-2*st.y, 1+2*st.y );
	float2 v = float2( (2-st.y) / vw.x - 1, (st.y)/vw.y + 1);
	v *= G_ProxyShadowmapTexture_TexelSize.y;

    half shadow;
	half sum = 0;
    sum += uw[0] * vw[0] * UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u[0], v[0]), coord.z, receiverPlaneDepthBias) );
    sum += uw[1] * vw[0] * UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u[1], v[0]), coord.z, receiverPlaneDepthBias) );
    sum += uw[0] * vw[1] * UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u[0], v[1]), coord.z, receiverPlaneDepthBias) );
    sum += uw[1] * vw[1] * UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u[1], v[1]), coord.z, receiverPlaneDepthBias) );

    shadow = sum / 16.0f;
    shadow = lerp (_LightShadowData.r, 1.0f, shadow);

    return shadow;
}

/**
 * PCF shadowmap filtering based on a 5x5 kernel (optimized with 9 taps)
 *
 * Algorithm: http://the-witness.net/news/2013/09/shadow-mapping-summary-part-1/
 * Implementation example: http://mynameismjp.wordpress.com/2013/09/10/shadow-maps/
 */
half sampleShadowmap_PCF5x5 (float4 coord, float2 receiverPlaneDepthBias)
{

#if defined(SHADOWS_NATIVE)

	const float2 offset = float2(0.5,0.5);
	float2 uv = (coord.xy * G_ProxyShadowmapTexture_TexelSize.zw) + offset;
	float2 base_uv = (floor(uv) - offset) * G_ProxyShadowmapTexture_TexelSize.xy;
	float2 st = frac(uv);

	float3 uw = float3( 4-3*st.x, 7, 1+3*st.x );
	float3 u = float3( (3-2*st.x) / uw.x - 2, (3+st.x)/uw.y, st.x/uw.z + 2 );
	u *= G_ProxyShadowmapTexture_TexelSize.x;

	float3 vw = float3( 4-3*st.y, 7, 1+3*st.y );
	float3 v = float3( (3-2*st.y) / vw.x - 2, (3+st.y)/vw.y, st.y/vw.z + 2 );
	v *= G_ProxyShadowmapTexture_TexelSize.y;

	half shadow;
	half sum = 0.0f;

	half3 accum = uw * vw.x;
	sum += accum.x * UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u.x,v.x), coord.z, receiverPlaneDepthBias) );
    sum += accum.y * UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u.y,v.x), coord.z, receiverPlaneDepthBias) );
    sum += accum.z * UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u.z,v.x), coord.z, receiverPlaneDepthBias) );

	accum = uw * vw.y;
    sum += accum.x *  UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u.x,v.y), coord.z, receiverPlaneDepthBias) );
    sum += accum.y *  UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u.y,v.y), coord.z, receiverPlaneDepthBias) );
    sum += accum.z *  UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u.z,v.y), coord.z, receiverPlaneDepthBias) );

	accum = uw * vw.z;
    sum += accum.x * UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u.x,v.z), coord.z, receiverPlaneDepthBias) );
    sum += accum.y * UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u.y,v.z), coord.z, receiverPlaneDepthBias) );
    sum += accum.z * UNITY_SAMPLE_SHADOW( G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2( base_uv, float2(u.z,v.z), coord.z, receiverPlaneDepthBias) );

    shadow = sum / 144.0f;

#else // #if defined(SHADOWS_NATIVE)

	// when we don't have hardware PCF sampling, then the above 5x5 optimized PCF really does not work.
	// Fallback to a simple 3x3 sampling with averaged results.

	half shadow = 0;
	float2 base_uv = coord.xy;
	float2 ts = G_ProxyShadowmapTexture_TexelSize.xy;
	shadow += UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2(base_uv, float2(-ts.x,-ts.y), coord.z, receiverPlaneDepthBias));
	shadow += UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2(base_uv, float2(    0,-ts.y), coord.z, receiverPlaneDepthBias));
	shadow += UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2(base_uv, float2( ts.x,-ts.y), coord.z, receiverPlaneDepthBias));
	shadow += UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2(base_uv, float2(-ts.x,    0), coord.z, receiverPlaneDepthBias));
	shadow += UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2(base_uv, float2(    0,    0), coord.z, receiverPlaneDepthBias));
	shadow += UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2(base_uv, float2( ts.x,    0), coord.z, receiverPlaneDepthBias));
	shadow += UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2(base_uv, float2(-ts.x, ts.y), coord.z, receiverPlaneDepthBias));
	shadow += UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2(base_uv, float2(    0, ts.y), coord.z, receiverPlaneDepthBias));
	shadow += UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture, P_UnityCombineShadowcoordComponents2(base_uv, float2( ts.x, ts.y), coord.z, receiverPlaneDepthBias));
	shadow /= 9.0;

#endif // else of #if defined(SHADOWS_NATIVE)

    return shadow;
}

#endif

/**
 *	Samples the shadowmap at the given coordinates.
 */
half unity_sampleShadowmap( float4 coord )
{
	half shadow = UNITY_SAMPLE_SHADOW(G_ProxyShadowmapTexture,coord);
	shadow = lerp(_LightShadowData.r, 1.0, shadow);
	return shadow;
}










// -Samples every shadow cascades without filtering
// -Eideren
half SamplePointFiltered(float4 sceneWorldPos)
{
    float3 vpos = mul(unity_WorldToCamera, sceneWorldPos);
	fixed4 cascadeWeights = GET_CASCADE_WEIGHTS(sceneWorldPos, vpos.z);
    float4 shadowCoord = GET_SHADOW_COORDINATES(sceneWorldPos, cascadeWeights);
    return unity_sampleShadowmap(shadowCoord);
}
// -Samples every shadow cascades and use unity's 5x5 PCF
// -Eideren
half SamplePCF5x5(float4 sceneWorldPos)
{
	// sample the cascade the pixel belongs to
    float3 vpos = mul(unity_WorldToCamera, sceneWorldPos);
	fixed4 cascadeWeights = GET_CASCADE_WEIGHTS(sceneWorldPos, vpos.z);
	float4 coord = GET_SHADOW_COORDINATES(sceneWorldPos, cascadeWeights);

#if UNITY_VERSION >= 201730
    float3 receiverPlaneDepthBias = 0.0;
#else
    float2 receiverPlaneDepthBiasCascade0 = 0.0;
    float2 receiverPlaneDepthBias = 0.0;
#endif
#ifdef UNITY_USE_RECEIVER_PLANE_BIAS
    // Reveiver plane depth bias: need to calculate it based on shadow coordinate
    // as it would be in first cascade; otherwise derivatives
    // at cascade boundaries will be all wrong. So compute
    // it from cascade 0 UV, and scale based on which cascade we're in.
    float3 coordCascade0 = getShadowCoord_SingleCascade(sceneWorldPos);
    #if UNITY_VERSION >= 201730
        receiverPlaneDepthBiasCascade0 = getReceiverPlaneDepthBias(coordCascade0.xyz);
    #endif
    float biasMultiply = dot(cascadeWeights,unity_ShadowCascadeScales);

    #if UNITY_VERSION >= 201730
        receiverPlaneDepthBias = UnityGetReceiverPlaneDepthBias(coordCascade0.xyz, biasMultiply);
    #else
        receiverPlaneDepthBias = receiverPlaneDepthBiasCascade0 * biasMultiply;
    #endif

    #if UNITY_VERSION < 201730
        // Static depth biasing to make up for incorrect fractional
        // sampling on the shadow map grid; from "A Sampling of Shadow Techniques"
        // (http://mynameismjp.wordpress.com/2013/09/10/shadow-maps/)
        float fractionalSamplingError = 2 * dot(G_ProxyShadowmapTexture_TexelSize.xy, abs(receiverPlaneDepthBias));
        coord.z -= min(fractionalSamplingError, UNITY_RECEIVER_PLANE_MIN_FRACTIONAL_ERROR);
    #endif
#endif


#if UNITY_VERSION >= 201730
    #if defined(SHADER_API_MOBILE)
        half shadow = UNITY_PCF5x5(coord, receiverPlaneDepthBias);
    #else
        half shadow = UNITY_PCF7x7(coord, receiverPlaneDepthBias);
    #endif
#else
    half shadow = sampleShadowmap_PCF5x5(coord, receiverPlaneDepthBias);
#endif
    // Blend shadowmap
    // smoothstep isn't required here, it can safely be removed at the cost of a slightly worse looking blend
    shadow = lerp(smoothstep(1, 0, getShadowmapEdgeMask(sceneWorldPos, vpos.z)), 1, shadow);
    shadow = lerp(_LightShadowData.r, 1, shadow);
	// Blend between shadow cascades if enabled
	//
	// Not working yet with split spheres, and no need when 1 cascade
#if UNITY_USE_CASCADE_BLENDING && !defined(SHADOWS_SPLIT_SPHERES) && !defined(SHADOWS_SINGLE_CASCADE)
	half4 z4 = (vpos.zzzz - _LightSplitsNear) / (_LightSplitsFar - _LightSplitsNear);
	half alpha = dot(z4 * cascadeWeights, half4(1,1,1,1));

	UNITY_BRANCH
		if (alpha > 1 - UNITY_CASCADE_BLEND_DISTANCE)
		{
			// get alpha to 0..1 range over the blend distance
			alpha = (alpha - (1 - UNITY_CASCADE_BLEND_DISTANCE)) / UNITY_CASCADE_BLEND_DISTANCE;

			// sample next cascade
			cascadeWeights = fixed4(0, cascadeWeights.xyz);
			coord = GET_SHADOW_COORDINATES(sceneWorldPos, cascadeWeights);

        #ifdef UNITY_USE_RECEIVER_PLANE_BIAS
            #if UNITY_VERSION >= 201730
                biasMultiply = dot(cascadeWeights,unity_ShadowCascadeScales);
                receiverPlaneDepthBias = UnityGetReceiverPlaneDepthBias(coordCascade0.xyz, biasMultiply);
            #else
                biasMultiply = dot(cascadeWeights,unity_ShadowCascadeScales);
                receiverPlaneDepthBias = receiverPlaneDepthBiasCascade0 * biasMultiply;
                fractionalSamplingError = 2 * dot(G_ProxyShadowmapTexture_TexelSize.xy, abs(receiverPlaneDepthBias));
                coord.z -= min(fractionalSamplingError, UNITY_RECEIVER_PLANE_MIN_FRACTIONAL_ERROR);
            #endif
        #endif
        #if UNITY_VERSION >= 201730
            half shadowNextCascade = UNITY_PCF3x3(coord, receiverPlaneDepthBias);
            shadowNextCascade = lerp(_LightShadowData.r, 1.0f, shadowNextCascade);
        #else
            half shadowNextCascade = sampleShadowmap_PCF3x3(coord, receiverPlaneDepthBias);
        #endif


			shadow = lerp(shadow, shadowNextCascade, alpha);
		}
#endif

	return shadow;
}


#endif // ifndef MODIFIED_SHADOW_SAMPLING