﻿using UnityEngine;
using UnityEngine.Rendering;
using CMDBuffer = UnityEngine.Rendering.CommandBuffer;

[ExecuteInEditMode]
public class ShadowmapIssuesWorkaround : MonoBehaviour
{
	const string shaderKeyword_DisableSSM      = "DISABLE_SHADOW_SSM";
	const string shaderName_ShadowMap          = "G_ProxyShadowmapTexture";
	const string shaderName_LightDir           = "G_SelectedLightDir";
	const string shaderName_ShadowDrawDistance = "G_ShadowDrawDistance";
	const string shaderName_ShadowCascades     = "G_ShadowCascades";


	public static Light 		selectedLight;
	public static RenderTexture unityShadowmap;
	public static RenderTexture proxyShadowmap;
		   static CMDBuffer     bufferPostShadowMap;
		   static CMDBuffer     bufferPreMask;
		   static CMDBuffer     bufferPostMask;


	static ShadowmapIssuesWorkaround instance;


	[Tooltip("Force shadow resolution one level above unity's base level ; very high resolution will render at 8192^2 instead of 4096^2")]
	public bool increaseMaxResolution = false;
	[Tooltip("Should better solve issues where Unity complains about texture resolution mismatch, if shadows seems to have rendering issues, disable it and PM me (Eideren) about it.")]
	public bool newResolutionChangeDetection = true;
	public bool logShadowMapMissing;
	public bool logErrorExplanation = true;


	/// <summary>
	/// Debug purposes only
	/// </summary>
	public RenderTexture unitySMProxy, proxySMProxy;

	void CleanupShadowmaps()
	{
		DiscardRT(ref proxyShadowmap);
		unityShadowmap = null;
	}


	static void DiscardRT(ref RenderTexture rt)
	{
		if(rt != null)
		{
			rt.Release();
			#if UNITY_EDITOR
			if(UnityEditor.EditorApplication.isPlaying || UnityEditor.EditorApplication.isPaused)
				Object.Destroy(rt);
			else
				Object.DestroyImmediate(rt);
			#else
			Object.Destroy(rt);
			#endif
			rt = null;
		}
	}

	void OnDisable()
	{
		if(instance != this)
			return;
		CleanupShadowmaps();
		DiscardCMDBuffer();
		Shader.SetGlobalVector(shaderName_LightDir, Vector4.zero);
		instance = null;
		selectedLight = null;
	}

	void DiscardCMDBuffer()
	{
		if(bufferPostShadowMap != null)
		{
			selectedLight.RemoveCommandBuffer(LightEvent.AfterShadowMap, bufferPostShadowMap);
			bufferPostShadowMap.Release();
			bufferPostShadowMap = null;
		}
		if(bufferPreMask != null)
		{
			selectedLight.RemoveCommandBuffer(LightEvent.BeforeScreenspaceMask, bufferPreMask);
			bufferPreMask.Release();
			bufferPreMask = null;
		}
		if(bufferPostMask != null)
		{
			selectedLight.RemoveCommandBuffer(LightEvent.AfterScreenspaceMask, bufferPostMask);
			bufferPostMask.Release();
			bufferPostMask = null;
		}
	}

	void LateUpdate()
	{
		ValidateData();
	}

	void OnDrawGizmos()
	{
		if(this.enabled)
			ValidateData();
		unitySMProxy = unityShadowmap;
		proxySMProxy = proxyShadowmap;
	}



	/* This is related to the multi light casting shadows issue, the issue being that unity doesn't reset the unity_WorldToShadow
	 * to the current light being rendered inside the pixel shader which in terms means that it uses the last-light-being-
	 * processed-through-the-shadow-mask's data instead.
	 * To properly fix this issue we would have to either grab those values inside or right after this light's shadow mask
	 * pass which would probably require us to write to a buffer inside the shader pass and then read from it later or
	 * compute the same data as unity does ( probably based on camera and light matrix, shadow distance, cascades and weights ) */
	/* Those methods where used to verify the claim above, sample was used before enabling other light sources to get
	 * the right data and send was used to replace unity's matrix data. */
	/*public System.Collections.Generic.List<Matrix4x4> m4x4 = new System.Collections.Generic.List<Matrix4x4>();
	[ExposeMethodInEditor]
	public void Sample()
	{
		m4x4[0] = Shader.GetGlobalMatrix("unity_WorldToShadow0");
		m4x4[1] = Shader.GetGlobalMatrix("unity_WorldToShadow1");
		m4x4[2] = Shader.GetGlobalMatrix("unity_WorldToShadow2");
		m4x4[3] = Shader.GetGlobalMatrix("unity_WorldToShadow3");
	}


	[ExposeMethodInEditor]
	public void Send()
	{
		Shader.SetGlobalMatrixArray("proxy_WorldToShadow", m4x4);
	}*/


	void ValidateData()
	{
		if(this.enabled == false)
			return;

		if (QualitySettings.shadows == ShadowQuality.Disable)
		{
			CleanupShadowmaps();
			return;
		}
		Shader.SetGlobalFloat(shaderName_ShadowDrawDistance, QualitySettings.shadowDistance);
		Shader.SetGlobalInt(shaderName_ShadowCascades, QualitySettings.shadowCascades);

		if(instance != null && instance != this)
		{
			Debug.LogError("Only one instance of type '"+typeof(ShadowmapIssuesWorkaround)+"' is allowed to run at the same time, we'll disable this one.");
			this.enabled = false;
			return;
		}
		if(instance == null)
			instance = this;

		if(selectedLight == null)
		{
			selectedLight = GetComponent<Light>();
			if(selectedLight == null)
			{
				Debug.LogError("Light missing");
				this.enabled = false;
				return;
			}
			if(selectedLight.type != LightType.Directional)
			{
				Debug.LogError(selectedLight.type+" not supported");
				this.enabled = false;
				return;
			}
		}
		if(selectedLight != null)
		{
			if (increaseMaxResolution)
				selectedLight.shadowCustomResolution = GetTextureSizeFromIndex(GetIndexFromInstanceResolution(selectedLight.shadowResolution)+1);
			else
				selectedLight.shadowCustomResolution = -1;
			// Send light dir to shader for comparison,
			// lightdir is inverted forward to stick to unity convention for _WorldSpaceLightPos0
			Vector4 lightDir = -selectedLight.transform.forward;
			lightDir.w = selectedLight.type == LightType.Directional ? 0 : 1;
			Shader.SetGlobalVector(shaderName_LightDir, lightDir);
			Shader.SetGlobalMatrix("TestLightMTRIX", selectedLight.transform.worldToLocalMatrix);
		}
		if(unityShadowmap == null)
		{
			// Unity's shadow map might discard itself if no
			// shadow casters fall within the shadow rendering distance
			// Discard the cmd buffer since it doesn't contain the right
			// rendertexture reference anymore after that event
			DiscardCMDBuffer();
			if(FetchUnityShadowmap(out unityShadowmap) == false)
			{
				if(logShadowMapMissing)
					Debug.LogError("Couldn't fetch shadowmap, you're probably too far way from any shadow caster.");
				return;
			}
		}

		if (newResolutionChangeDetection)
		{
			int predictedShadowResolution;
			if (selectedLight.shadowCustomResolution > -1)
				predictedShadowResolution = selectedLight.shadowCustomResolution;
			else
				predictedShadowResolution = GetTextureSizeFromInstanceResolution(selectedLight.shadowResolution);
			// Unity takse a couple of frame before applying a new shadow quality so we'll have to wait until unity has applied those changes
			// or else a bunch of errors will pop up with the command buffers
			if (unityShadowmap.width != predictedShadowResolution)
			{
				DiscardCMDBuffer();
				return;
			}
		}

		if(proxyShadowmap != null && (proxyShadowmap.texelSize != unityShadowmap.texelSize))
		{
			DiscardRT(ref proxyShadowmap);
			DiscardCMDBuffer();
			if(logErrorExplanation && !newResolutionChangeDetection)
				Debug.Log("Shadow resolution changed, ignore Graphics.CopyTexture errors, they are harmless. "
				+"\nWe don't have the ability to update our values at the right time, sorry but you'll have to deal with those errors showing up.");
		}
		if(proxyShadowmap == null)
		{
			#if UNITY_2017_1_OR_NEWER
			proxyShadowmap = new RenderTexture(unityShadowmap);
			#else
			proxyShadowmap = RTNewAndCopyParams(unityShadowmap);
			#endif
			proxyShadowmap.name = "proxyShadowmap:"+selectedLight.name;
			proxyShadowmap.Create();
			proxyShadowmap.hideFlags = HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;
			#if UNITY_EDITOR && UNITY_2017_1_OR_NEWER
			// Force unity to cleanup RTs after compilation; unity doesn't bind back static fields since it doesn't
			// serialize them. After each serialization the field is null so we re-create a texture but must have a
			// way to discard our hold, unbound one so we bind any new rt to a delegate and discard it once it runs.
			UnityEditor.AssemblyReloadEvents.beforeAssemblyReload += delegate { DiscardRT(ref proxyShadowmap); };
			#endif
			Shader.SetGlobalTexture(shaderName_ShadowMap, proxyShadowmap);
		}

		if(bufferPostShadowMap == null)
		{
			bufferPostShadowMap = new CMDBuffer();
			bufferPostShadowMap.name = typeof(ShadowmapIssuesWorkaround).Name;
			// this event with BuiltinRenderTextureType.CurrentActive as a source is kind of buggy, the source reports
			// a smaller texture than expected in certain cases, I found that selecting the camera and hiding any inspector
			// focused on this component was the easiest way to reproduce this issue.
			bufferPostShadowMap.CopyTexture(unityShadowmap, proxyShadowmap);
			selectedLight.AddCommandBuffer(LightEvent.AfterShadowMap, bufferPostShadowMap);
		}
		if(bufferPreMask == null)
		{
			bufferPreMask = new CMDBuffer();
			bufferPreMask.name = typeof(ShadowmapIssuesWorkaround).Name;
			bufferPreMask.EnableShaderKeyword(shaderKeyword_DisableSSM);
			selectedLight.AddCommandBuffer(LightEvent.BeforeScreenspaceMask, bufferPreMask);
		}
		if(bufferPostMask == null)
		{
			bufferPostMask = new CMDBuffer();
			bufferPostMask.name = typeof(ShadowmapIssuesWorkaround).Name;
			bufferPostMask.DisableShaderKeyword(shaderKeyword_DisableSSM);
			selectedLight.AddCommandBuffer(LightEvent.AfterScreenspaceMask, bufferPostMask);
		}

	}

	#if UNITY_2017_1_OR_NEWER
	#else
	RenderTexture RTNewAndCopyParams(RenderTexture rt)
	{
		return new RenderTexture(rt.width, rt.height, rt.depth, rt.format, rt.sRGB ? RenderTextureReadWrite.sRGB : RenderTextureReadWrite.Linear)
		{
			name = rt.name,
			antiAliasing = rt.antiAliasing,
			autoGenerateMips = rt.autoGenerateMips,
			dimension = rt.dimension,
			enableRandomWrite = rt.enableRandomWrite,
			useMipMap = rt.useMipMap,
			volumeDepth = rt.volumeDepth
		};
	}
	#endif




	bool FetchUnityShadowmap(out RenderTexture rt)
	{
		foreach(Object obj in Resources.FindObjectsOfTypeAll(typeof(RenderTexture)))
		{
			if(obj.name == "Shadowmap")
			{
				rt = (RenderTexture)obj;
				return true;
			}
		}
		rt = null;
		return false;
	}


	static int GetTextureSizeFromInstanceResolution(LightShadowResolution lsr)
	{
		return GetTextureSizeFromIndex(GetIndexFromInstanceResolution(lsr));
	}
	static int GetTextureSizeFromGlobalResolution(ShadowResolution sRes)
	{
		return GetTextureSizeFromIndex(GetIndexFromShadowResolution(sRes));
	}


	const int unityBaseShadowResolution = 512;
	static int GetTextureSizeFromIndex(int index)
	{
		switch (index)
		{
			case 0://Low
				return unityBaseShadowResolution;
			case 1://Medium
				return unityBaseShadowResolution * 2;
			case 2://High
				return unityBaseShadowResolution * 4;
			case 3://VeryHigh
				return unityBaseShadowResolution * 8;
			case 4://VeryHigh+1
				return unityBaseShadowResolution * 16;
			default:
				throw new System.NotImplementedException();
		}
	}




	static int GetIndexFromShadowResolution(ShadowResolution sRes)
	{
		switch (sRes)
		{
			case ShadowResolution.Low:
				return 0;
			case ShadowResolution.Medium:
				return 1;
			case ShadowResolution.High:
				return 2;
			case ShadowResolution.VeryHigh:
				return 3;
			default:
				throw new System.NotImplementedException();
		}
	}
	static int GetIndexFromInstanceResolution(LightShadowResolution lsr)
	{
		switch (lsr)
		{
			case LightShadowResolution.FromQualitySettings:
				return GetIndexFromShadowResolution(QualitySettings.shadowResolution);
			case LightShadowResolution.Low:
				return GetIndexFromShadowResolution(ShadowResolution.Low);
			case LightShadowResolution.Medium:
				return GetIndexFromShadowResolution(ShadowResolution.Medium);
			case LightShadowResolution.High:
				return GetIndexFromShadowResolution(ShadowResolution.High);
			case LightShadowResolution.VeryHigh:
				return GetIndexFromShadowResolution(ShadowResolution.VeryHigh);
			default:
				throw new System.NotImplementedException();
		}
	}
}