As seen in [this link](http://i.imgur.com/Wkh6iaM.png), Unity doesn't solve shadows on MSAA'ed pixels since it computes a shadow mask on the depth buffer.

What this repo does is force unity to sample the shadow map inside geometries 's fragment pass instead so that the fragment properly evaluate shadows for each MSAA sample.

Pseudo code showing how I solve this issue :
```
OnRenderShadowmap:
->Grab shadow map
  \_Copy to proxy render texture
   \_Disable shadow mask rendering for selected light since we wont be using it
Standard-FragmentPass:
->Override unity's *shadow-mask* sample function by our *shadowmap* sample for the selected light only
```

If you want to implement this fix for your own shaders, you can just #include "ModifiedShadowSampling.cginc" at the top of your
include list to properly override unity's function and then sample your shadows using unity's function.