﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameobjectGrid : MonoBehaviour
{
	public Vector3 grid = new Vector3(25, 25, 25);
	public float offset = 1.5f;
	public Material setCustomMaterial;

	void Start()
	{
		Transform gridParent = new GameObject("Grid Parent").transform;
		for(int x = 0; x < grid.x; x++)
		{
			for(int y = 0; y < grid.x; y++)
			{
				for(int z = 0; z < grid.x; z++)
				{
					GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
					go.transform.position = new Vector3(x, y, z)*offset;
					go.transform.SetParent(gridParent);
					if(setCustomMaterial != null)
						go.GetComponent<MeshRenderer>().sharedMaterial = setCustomMaterial;
					Destroy(go.GetComponent<Collider>());
				}
			}
		}
	}
	
}
